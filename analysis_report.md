Occupational Therapy in Home Health Analysis Report
================

View this [information online (mobile
friendly)](https://gitlab.com/ryanlabs/othh/blob/master/analysis_report.md)
with access to all [code](https://www.gitlab.com/ryanlabs/othh) and
[data](https://gitlab.com/ryanlabs/othh/blob/master/README.md#data)

*If using on mobile or online, click on any image to zoom.*

## Objectives

*The* ***primary*** *objective of this analysis is to demonstrate skills
and provide auditable code. The analyses look at the OT perspective in
Home Health from publically available data. I selected home health
because it is the next area to be addressed by the [Volume to
Value](https://www.aota.org/value) project.*

1.  Examine patterns of OT utilization across 2013-2016 at the
    oraganization level and compare utilization of OT to other therapy
    disciplines
2.  Explore characteristics of home health agencies that use OT to those
    that do not
3.  Examine geographic differences in the use of OT (potentially related
    to small area variations)
4.  Examine relationship between OT visits provided by agencies and the
    agencies’ score on star rating quality measures

## Assumptions and Limitations

The following assumptions and limitations should be considered:

  - LUPA episodes are **not** included in the data. (A Low Utilitzation
    Payment Adjustment (LUPA) occurs when 4 or fewer total visits occur
    in a 60 day episode)
  - Only data with at least 10 observations are included
  - Data are provided only in aggregate form
  - Utilization data are delayed by two years (2016 is the most recent
    data available)
  - Quality data are released much more frequency (typically multiple
    times in the same year)

We are underestimating total visits by all disciplines. But, this is the
best publically available data. All calculations should be considered
estimates and not abosolute. This initial exploratory data analysis is
meant to generate additional questions, discussion and quantify the
magnitude of opportunities and threats for occupational therapy in home
health.

## Analysis and Code

This document was created in Rmarkdown. The [Rmarkdown
file](https://gitlab.com/ryanlabs/othh/blob/master/analysis_report.Rmd)
contains the code to generate each of the plots, but it has been
suppressed to make this document easier to read. The information
presented here is not predictive; it is exploratory in nature. *The
results cannot be used to generalize or predict future performance.
Instead these exploratory analyses can inform more rigorous, time
intensive statistical analyses in the future if warranted.*

**Check the readme, see all code, and data at
<https://gitlab.com/ryanlabs/othh>**

  - Click [here to see the code that created this
    document](https://gitlab.com/ryanlabs/othh/blob/master/analysis_report.Rmd)

# Provider Level Descriptive Analysis

All analyses in this section is at the provider level. *Note that we
cannot intepret this information at the individual client level.*

## Objective 1: Examine Patterns of OT Utilization

### Total Visits by Discipline

![](analysis_report_files/figure-gfm/unnamed-chunk-1-1.png)<!-- -->

PT was used substantially more often in home health than OT. OT was used
substantially more often than ST. From 2013-2016, the total number of OT
visits grew each and every year.

### Average Visits by Discipline

![](analysis_report_files/figure-gfm/unnamed-chunk-2-1.png)<!-- -->

There was an average of 1 OT visit per episode across agencies. There
was an average of approximately 5 PT visits per episode.

#### Table 1: Average therapy visits by discipline and year

| year | Avg OT Visits | Avg PT Visits | Avg ST Visits |
| ---: | ------------: | ------------: | ------------: |
| 2013 |          0.99 |          5.01 |          0.17 |
| 2014 |          1.04 |          5.08 |          0.17 |
| 2015 |          1.12 |          5.22 |          0.19 |
| 2016 |          1.23 |          5.35 |          0.21 |

From 2013 to 2016, OT average visits per episode grew by 24%. For
comparison, PT grew by approximately 7%. In 2013, there were 5.9 times
more PT visits than OT. In 2016, there were 4.3 times more PT visits
than OT on average per episode.

### Occupational Therapy Visit Distributions

![](analysis_report_files/figure-gfm/unnamed-chunk-4-1.png)<!-- -->

*Interpretting the boxplot*: The large box shows the middle 50% of the
data if we ignore the outliers. The dark line in the box shows the
median for each year. The thin solid lines extending above and below the
box show the top and bottom 25% of the data respectively, and the dots
(in this case all above the thin lines) show the outliers. We can see
from this plot that there are quite a few outliers on the high side. The
average of OT visits per agency is not normally distrubted.

**Observations.** Note that the median average number of OT visits
increases each year. The boxes also shift up each year which means that
the median is not shifting solely because of outliers. But, *most*
providers are providing more OT each year.

Another way to look at this is the quantiles for average number of OT
visits and total number of OT visits. In the tables below, we show the
minimum, 25th percentile, median, 75th percentile, and maximum of each
**for visits in 2013 and visits in 2016 on average.**

#### Table 2: OT visit quantiles for 2013

|         | Min | X25th | Median | X75th |      Max |
| ------- | --: | ----: | -----: | ----: | -------: |
| Average |   0 |   0.1 |    0.7 |   1.5 |     12.4 |
| Total   |   0 |  24.6 |  148.0 | 646.5 | 74,771.2 |

#### Table 3: OT visit quantiles for 2016

|         | Min | X25th | Median | X75th |      Max |
| ------- | --: | ----: | -----: | ----: | -------: |
| Average |   0 |   0.2 |    0.9 |   1.9 |     10.9 |
| Total   |   0 |  37.5 |  207.6 | 898.2 | 47,311.2 |

There were 2 astronomical points for 2013 and 2014 in the boxplot (\#3)
above. These tables show that the very high points do not shift the
quantiles of the visits. Even though the maximum average and maximum
number of visits were both higher in 2013, the 25th percentile, median,
and 75th percentile of visits were all higher in 2016.

## Objective 2: Explore Characteristics of Agencies that use OT

Not all home health agencies provide occupational thearpy. We will look
at those that did and those that did not.

### Home Health Providers

![](analysis_report_files/figure-gfm/unnamed-chunk-7-1.png)<!-- -->

The number of indidivual agencies decreased year over year. The number
of agencies that did **not** provide OT **decreased** each year at a
faster rate. Note the blue portion of each bar gets considerably smaller
each year. There is a consistent pattern of more agencies providing OT
to beneficiaries.

#### Table 4: How many agencies did <u>not</u> provide OT by year?

| year | Number | Percent |
| ---: | -----: | ------: |
| 2013 |   1845 |    16.7 |
| 2014 |   1700 |    15.6 |
| 2015 |   1567 |    14.9 |
| 2016 |   1371 |    13.5 |

In 2013, 1,845 agencies (16.7% of the total number of agencies) did not
provide OT. But, in 2016, that number decreased to 1,371 (13.5%). This
is good news for OT. **Each year, more total visits are provided by
agencies. And, each year, a larger percentage of home health agencies
provide OT.**

#### Table 5: Differences in agencies that provide OT and agencies that do not

|                    | noOT | Female | Dual | White | Beneficiaries | Episodes |     Payment |  HCC |  Alz |
| ------------------ | ---: | -----: | ---: | ----: | ------------: | -------: | ----------: | ---: | ---: |
| Provided OT        |    0 |   0.62 | 0.38 |  0.72 |        369.01 |   623.88 | 1,897,141.3 | 2.25 | 0.25 |
| Did Not Provide OT |    1 |   0.60 | 0.56 |  0.61 |        102.07 |   226.12 |   598,282.7 | 2.22 | 0.24 |

*Female, Dual, White, and Alz are percentages*

Keeping in mind that these are not **statistical** differences, we
explore some potential differences and similarities between agencies
that provide OT and those that do not across all 4 years. It appears
that agencies that did not provide OT had more dual beneficiaries
meaning that more of their clients had Medicaid. It also appears they
saw less white beneficiaries, provided fewer total home health episodes,
and were paid less by Medicare. Interestingly, the HCC, a score that
indicates the comorbidities of beneficiaries, is very similar between
the two as is the percent with Alzheimer’s disease and the percent of
female beneficiaries.

We have more data on diagnoses. We use those with Alzheimer’s disease as
an example. With more time, we can explore many more attributes.

## Objective 3: Geographic Differences in the Use of OT

A limitation of the data available is that state is by agency location,
not necessarily where services are provided. Nonetheless, it can be
illustrative to examine the differences by state. *Note:* The plots in
this section are aesthetically different than plots elsewhere for ease
of reading and interpretation.

### Average OT Visits by State

![](analysis_report_files/figure-gfm/unnamed-chunk-10-1.png)<!-- -->

There is a noticable variation in average number of vitis in an episode
by state. While not universal, agencies in the South tend to have fewer
OT visits per episode while agencies in the Northcentral US tend to have
more OT visits per episode. While visits per episode may be helpful to
understand potential growth areas, examining total visits per state may
provide more information to understand OT practitioner employment in
home health.

### Total OT Visits by State

![](analysis_report_files/figure-gfm/unnamed-chunk-11-1.png)<!-- -->

By taking a look at the total visits in each state, we see a more
complete picture. While SD sets the record for the most visits of OT per
episode at 2.51 visits per episode, the state is very near the bottom of
total visits at 57,096 estimated visits. FL and TX lead the total visits
with 4,088,883 and 3,083,633 respectively. The average visits per
episode are 1.25 in FL and only 0.65 in TX.

Keep in mind that these data include all 4 years combined (2013-2016).
The results are not shown here, but sensitivity analyses were completed
using data from each year to look at trends. FL was \#1 and TX was \#2
in total visits each year.

**Region:** We see many more states in the South closer to the top of
total visits and at the bottom for average visits per episode. This
brings up the question: Is there potential for job market growth in the
South? OT practitioners are seeing more clients here, but are seeing
them less frequently than in other areas of the country.

## Objective 4: Relationships between OT Visits and Quality Measures

For these analyses, we combined data from the Home Health Star Rating
Quality Measures and total OT provided by the agency. Remember that
these data are at the agency level, *not* at the beneficiary level. Our
question: “Is there a potential relationship between the amount of total
OT visits *provided by an agency* and the *quality measure scores of the
agency*? While not quite perfect, this could be thought of as”Do
agencies that provide more OT visits also score higher on quality
measures?"

#### Quality Measures and OT Visits in 2016

We show a plot of each star quality measure and OT visits below. There
are quite a few of them. SEe Figure 1 for an overview of reading the
plots using the **percent of beneficiaries who improve in bathing**
plot.

<img src="plots/impr_bath_arrow.png" title="Orientation to reding measure plots" alt="Orientation to reding measure plots" width="50%" />

As we look at each measure, note that dots more to the right of the plot
indicate a home health agency with a higher score on the quality
measure. The position on the x axis indicates the estimated percentage
of beneficiaries treated by an agency who triggered the quality measure.
This is represented by the red arrow above. Dots higher on the y axis
indicates a home health agency with more OT visits. This is represented
by the green arrow above.

![](analysis_report_files/figure-gfm/unnamed-chunk-13-1.png)<!-- -->![](analysis_report_files/figure-gfm/unnamed-chunk-13-2.png)<!-- -->

Note that the Total OT Visits are displayed in log2 due to the very
large count numbers. This can be interpreted as for a 1 unit of increase
on the y axis the total number of OT visits double. There are no strong
relationships between the primary quality measures reported in 2016 and
total OT visits. This is expected in some of the measures displayed such
as the percentage of beneficiaries in an agency that receive
vaccinations for flu or pneumonia.

These are not likely the best quality measures to capture the
contribution of OT in home health.

We can identify some opportunities for OT. For example, **in the top
right chart, depression\_screen,** we see some agencies that have
relatively high OT visits score low on the depression screen measure.
Note that there are quite a few dots between 5 and 10 on total OT visits
(y axis) that correspond to less than 50% of the beneficiaries receiving
a screen for depression.

These plots represent completely unadjusted numbers. We have enough data
to explore regression and adjustment for the measures, but time did not
allow for this report. We can also explore results of the HCAPS measures
which are patient reported and patient experience measures.

## Conclusion

### Importance & Policy

As of this data analysis the number of OT visits contributes to the
overall reimbursement an agency receives. See below. The total OT visits
tracks with the total Medicare payment across all 4 years.

![](analysis_report_files/figure-gfm/unnamed-chunk-14-1.png)<!-- -->

**However,** as of January 1, 2020, the new **Patient Driven Groupings
Model (PDGM)** will be implemented. Under PDGM, the number of OT visits
no longer contribute to the total payment an agency receives. It is
extremely important to understand the trends of OT utilization leading
up to and following the payment change. From 2013-2016, the total
number, the median number, and the average number per episode of OT
visits cotninually increased.

### Take Aways from the Analyses from 2013-2016

  - It appears that more home health agencies provided OT in 2016
    compared to 2013. They also appear to have provided more total OT
    visits and more OT visits on average to beneficiaries.
  - OT appears to have grown at a faster rate than PT or ST.
  - There is some evidence that agencies that provided OT serve
    different beneficiaries than those that did not provide OT.
  - There may be geographic factors and variations in the provision of
    OT services
  - While there is no immediately apparent relationship between the
    amount of OT provided and the score on quality measures, more
    rigorous analyses is needed to understand how confounding factors
    may relate.
  - The number of OT visits appear to have correlated with Medicare
    reimbursement. Following these trends up to and after the
    implementation of PDGM will be important.

### Additional Questions and Next Steps

*Exploratory data analysis should elicit additional questions for
stakeholders. The first next step is to get feedback from stakeholders
with various expertise. This should include soliciting questions from
each practice and policy group, member experience, and operations.*
Other recommended next steps include:

  - Review initial analyses with stakeholder to garner additional
    quesitons
  - Consider regression and statistical analysis of quality measures
    over the entire timeframe and by year
  - Explore relationships of Total or Average OT visits on chronic
    condition categories, ownership type, and other potential variables
    including home health resource group (HHRG) prevalence.
