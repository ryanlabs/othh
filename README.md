# Occupational Therapy (OT) in Home Health (HH)
**Click ["analysis_report.md"](https://gitlab.com/ryanlabs/othh/blob/master/analysis_report.md) to review the summary of the exploratory data analysis.** 

* Review the imported data from CMS in the [data](https://gitlab.com/ryanlabs/othh/tree/master/data) folder on gitlab. The data files are provided ***only*** for use to audit the code in this project. For any other use, the data should be downloaded directly from CMS at the links provided below under [Data](https://gitlab.com/ryanlabs/othh/tree/master#data).
* See the saved data frames from R in [rda](https://gitlab.com/ryanlabs/othh/tree/master/rda) (RData)
* All scripts and code for data are available in [gitlab](https://www.gitlab.com/ryanlabs/othh)

## Purpose
This project is a skills demonstration project. It is an exploratory data analysis (EDA) of rehabilitation services in home health using Medicare PUF. *All data is publically available at [data.cms.gov](https://data.cms.gov).*

## Data
Data should be downloaded directly from CMS at the links provided below.
* [Home Health Compare Revised Flatfiles published 2016-10-19](https://data.medicare.gov/data/archives/home-health-compare)
* [Medicare Home Health Provider Aggregate Table, CY 2013](https://data.cms.gov/Medicare-Home-Health-Agency-HHA-/Medicare-Home-Health-Provider-Aggregate-Table-CY-2/rbzt-mt5u)
* [Medicare Home Health Agency (HHA) Provider Aggregate Report, CY 2014](https://data.cms.gov/Medicare-Home-Health-Agency-HHA-/Medicare-Home-Health-Agency-HHA-Provider-Aggregate/mmse-e9yd)
* [Medicare Home Health Agency (HHA) Provider Aggregate Report, CY 2015](https://data.cms.gov/Medicare-Home-Health-Agency-HHA-/Medicare-Home-Health-Agency-HHA-Provider-Aggregate/5vaz-czzq)
* [Medicare Home Health Agency (HHA) Provider Aggregate Report, CY 2016](https://data.cms.gov/Medicare-Home-Health-Agency-HHA-/Medicare-Home-Health-Agency-HHA-Provider-Aggregate/fdg8-v2i5)

## Software & Analyses
All analyses were completed using [R](https://cran.r-project.org/), [RStudio](https://www.rstudio.com/products/rstudio/), and the [tidyverse](https://www.tidyverse.org/). The results are posted to [gitlab](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/). Each is open source or open core. 

## Objectives
1. Examine patterns of OT utilization across 2013-2016 at the oraganization level and compare utilization of OT to other therapy disciplines
2. Explore characteristics of home health agencies that use OT to those that do not
3. Examine geographic differences in the use of OT (potentially related to small area variations)
4. Examine relationship between OT visits provided by agencies and the agencies' score on star rating quality measures